package com.sigmotoa;

public class Triangle extends Geometric {
    public Triangle(String figurename, int sidesnumber, double side) {
        super(figurename, sidesnumber, side);
    }

    @Override
    public void areaCalculation()
    {
        area = (this.area*this.area)/2;

    }

    @Override
    public void perimeterCalculation()
    {

    }
}

